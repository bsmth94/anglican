(ns anglican.forward-diff-test
  (:require [clojure.test :refer [deftest testing is run-tests]]
            [clojure.pprint :refer [pprint]])
  (:use anglican.forward-diff))


(deftest test-diff-expr
  (binding [*gensym* symbol]

    (testing "plus"
      (is (= (diff-expr '(+ x y z 3) '{x dx, y dy, z dz})
             '[(+ x y z 3) (+ dx dy dz)])))

    (testing "times"
      (is (= (diff-expr '(* x y z 3) '{x dx y dy z dz})
             '[(* x y z 3) (+ (* dx y z 3) (* x dy z 3) (* x y dz 3))])))

    (testing "div"
      (is (= (diff-expr '(/ x y 3 4 z) '{x dx y dy z dz})
             '[(/ x y 3 4 z) (+ (* dx (/ 1 y 3 4 z))
                                (* dy (/ x 3 4 z 1) (pow y -2))
                                (* dz (/ x y 3 4 1) (pow z -2)))])))

    (testing "fn*"
      (is (= (diff-expr '( (fn* myfn [x y] (* x (+ x y)))
                           a b)
                        '{a da b db})

             '((fn* dmyfn [x y dx dy]
                    (let [tmp1 (+ x y)
                          tmp2 (+ dx dy)]
                      [(* x tmp1) (+ (* dx tmp1) (* x tmp2))]))
               a b da db))))

    (testing "loop*-recur"
      (is (= (diff-expr '(loop* [i 0
                                 x (+ 1 a)]
                                (if (= i 10)
                                  x
                                  (recur (+ i 1)
                                         (* x 2))))
                        '{a da})

             '(loop [[i di] [0 0]
                     [x dx] [(+ 1 a) da]]
                (if (= i 10)
                  [x dx]
                  (recur [(+ i 1) di]
                         [(* x 2) (* dx 2)]))))))

    (testing "if"
      (is (= (diff-expr '(if (< 100 200)
                           (+ x y)
                           (- x y))
                        '{x dx, y dy})
             '(if (< 100 200)
                [(+ x y) (+ dx dy)]
                [(- x y) (- dx dy)]))))


    (testing "let*"
      (is (= (diff-expr '(let* [y (* 2 (exp x))
                                z (+ x y)]
                               (* y z))
                        '{x dx})

             '(let [ [y dy] (let [tmp1 (exp x)
                                  tmp2 (* dx (exp x))]
                              [(* 2 tmp1) (* 2 tmp2)])
                     [z dz] [(+ x y) (+ dx dy)]]
                [(* y z) (+ (* dy z) (* y dz))]))))


    (testing "let*-and-fn*"
      (is (= (diff-expr '(let* [f (fn* [x y] (* x y))]
                               (f a (* a a)))
                        '{a da})
             '(let [ [f df] [(fn* FN [x y] (* x y))
                             (fn* dFN [x y dx dy]
                                  [(* x y)
                                   (+ (* dx y) (* x dy))])]]
                (df a (* a a) da (+ (* da a) (* a da)))))))

    (testing "cond"
      (is (= (diff-expr '(cond (> x 0) (+ x y)
                               (> x 1) (- x y)
                               (> x 2) (* x y)
                               :else (/ x y))
                        '{x dx, y dy})
             '(cond (> x 0) [(+ x y) (+ dx dy)]
                    (> x 1) [(- x y) (- dx dy)]
                    (> x 2) [(* x y) (+ (* dx y) (* x dy))]
                    :else [(/ x y) (+ (* dx (/ 1 y)) (* dy (/ x 1) (pow y -2)))]))))



    (testing "lists-vectors"
      (is (= (diff-expr '(list (+ x y z) [(/ x 2) (/ y 3)] (vector z 5))
                        '{x dx, y dy, z dz})
             '[(list (+ x y z) (vector (/ x 2) (/ y 3)) (vector z 5))
               (list (+ dx dy dz) (vector (* dx (/ 1 2)) (* dy (/ 1 3))) (vector dz 0))])))

    (testing "let*-first-second-next-nth-rest"
      (is (= (diff-expr '(let* [lst (list x (* x 2) (* x 3) (* x 4))]
                               (vector
                                 (first lst)
                                 (second lst)
                                 (next lst)
                                 (nth lst 3)
                                 (rest lst)))
                        '{x dx})

             '(let [[lst dlst] [(list x (* x 2) (* x 3) (* x 4))
                                (list dx (* dx 2) (* dx 3) (* dx 4))]]

                [(vector (first lst) (second lst) (next lst) (nth lst 3) (rest lst))
                 (vector (first dlst) (second dlst) (next dlst) (nth dlst 3) (rest dlst))]))))

    (testing "repeatedly"
      (is (= (diff-expr '(repeatedly 1000 (fn* FN [] (pow x x))) '{x dx})
             '(let [results (repeatedly 1000 (fn* dFN []
                                                  [(pow x x) (+ (* dx x (pow x (- x 1)))
                                                                (* dx (pow x x) (log x)))]))]
                [(map first results) (map second results)]))))


    (comment

      (testing "apply-anon"
        (is (= (diff-expr '(apply (fn f [x y z] (* x y z))
                                  (list a (* 2 a) (* 3 a)))
                          '{a da})
               '(apply
                  (fn df [x y z dx dy dz] [(* x y z)
                                           (+ (* dx y z) (* x dy z) (* x y dz))])
                  (concat (list a (* 2 a) (* 3 a))
                          (list da (* 2 da) (* 3 da)))))))



      (testing "apply-plus-repeat"
        (is (= (diff-expr '(apply + (repeat 10 (* x x)))
                          '{x dx})

               '(if (= (count (repeat 10 (* x x))) 1)
                  ((fn [arg darg] [arg darg])
                   (first (repeat 10 (* x x)))
                   (first (repeat 10 (+ (* dx x) (* x dx)))))

                  (reduce (fn [[result dresult] [arg darg]]
                            [(+ arg result) (+ darg dresult)])
                          (map vector
                               (repeat 10 (* x x))
                               (repeat 10 (+ (* dx x) (* x dx)))))))))

      (testing "apply-times-list"
        (is (= (diff-expr '(apply * (list x y z))
                          '{x dx y dy z dz})
               '(if (= (count (list x y z)) 1)

                  ((fn [arg darg] [arg darg])
                   (first (list x y z)) (first (list dx dy dz)))

                  (reduce
                    (fn [[result dresult] [arg darg]]
                      [(* arg result) (+ (* darg result) (* arg dresult))])
                    (map vector (list x y z) (list dx dy dz)))))))

      (testing "apply-pow"
        (is (= (diff-expr '(apply pow (list x y))
                          '{x dx y dy})
               '(if (= (count (list x y)) 1)
                  ((fn [arg darg] [(pow arg nil) (+ (* darg nil (pow arg (- nil 1)))
                                                    (* nil (pow arg nil) (log arg)))])
                   (first (list x y)) (first (list dx dy)))

                  (reduce
                    (fn [[result dresult] [arg darg]]
                      [(pow arg result)
                       (+ (* darg result (pow arg (- result 1)))
                          (* dresult (pow arg result) (log arg)))])
                    (map vector (list x y) (list dx dy)))))))

      (testing "apply-exp"
        (is (= (diff-expr '(apply exp (list x))
                          '{x dx})
               '(if (= (count (list x)) 1)
                  ((fn [arg darg] [(exp arg) (* darg (exp arg))])
                   (first (list x))
                   (first (list dx)))
                  (reduce
                    (fn [[result dresult] [arg darg]]
                      [(exp arg) (* darg (exp arg))])
                    (map vector (list x) (list dx)))))))

      )


    (testing "map-times"
      (is (= (diff-expr '(map * (list x y z) (list z y x))
                        '{x dx y dy z dz})
             '(map (fn
                     [tmp1 tmp2 dtmp1 dtmp2]
                     [(* tmp1 tmp2) (+ (* dtmp1 tmp2) (* tmp1 dtmp2))])
                   (list x y z)
                   (list z y x)
                   (list dx dy dz)
                   (list dz dy dx)))))

    (testing "map-anon"
      (is (= (diff-expr '(map (fn* [x y] (/ x y)) (list a 2) (list 3 b))
                        '{a da b db})
             '(map (fn* dFN [x y dx dy]
                        [(/ x y) (+ (* dx (/ 1 y)) (* dy (/ x 1) (pow y -2)))])
                   (list a 2)
                   (list 3 b)
                   (list da 0)
                   (list 0 db)))))

    (testing "reduce-div"
      (is (= (diff-expr '(reduce / r (list x (* 2 x) (* 3 x)))
                        '{x dx r dr})
             '(reduce
                (fn [[result dresult] [arg darg]]
                  [(/ arg result)
                   (+ (* darg (/ 1 result))
                      (* dresult (/ arg 1) (pow result -2)))])
                [r dr]
                (map vector
                     (list x (* 2 x) (* 3 x))
                     (list dx (* 2 dx) (* 3 dx)))))))

    (testing "reduce-anon"
      (is (= (diff-expr '(reduce (fn* [a b] (pow a b))
                                 (list x m n p))
                        '{x dx m dm n dn p dp})
             '(reduce
                (fn [[result dresult] [arg darg]]
                  (
                    (fn* dFN [a b da db]
                         [(pow a b) (+ (* da b (pow a (- b 1))) (* db (pow a b) (log a)))])
                    result dresult
                    arg darg))
                (map vector
                     (list x m n p)
                     (list dx dm dn dp))))))


    (testing "filter"
      (is (= (diff-expr '(filter (fn [a] (> a 0)) (list (- x) 0 1 2 x (* x x))) '{x dx})

             '(let [filtered (filter (fn [[arg darg]] ((fn [a] (> a 0)) arg))
                                     (map vector
                                          (list (- x) 0 1 2 x (* x x))
                                          (list (- dx) 0 0 0 dx (+ (* dx x) (* x dx)))))]
                [(map first filtered) (map second filtered)]))))


    ))


;(run-tests)
