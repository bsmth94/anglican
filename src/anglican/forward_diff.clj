(ns anglican.forward-diff
  "Forward-mode autodifferentiation"
  (:require [clojure.pprint :refer [pprint]]))


(declare funcs-map) ;; a map going from primitive functions to their derivatives
(declare special-map) ;; a map going from special functions to their derivatives
(declare diff-term) ;; differentiates a single symbol


(def ^:dynamic *gensym*
  "customized gensym for code generation,
  bound to `symbol' in tests"
  gensym)

(defn my-gensym [& args]
  "just a version of gensym wrapping the above that takes
  multiple arguments and concatenates them (convenient)"
  (*gensym* (apply str args)))





;; diff-expr is the central function of this file. It differentiates a number of
;; important types of expressions.

(defn diff-expr [code inputs-map]

  ;; First, there's the possibility that 'code' is actually just a single term. That's easy.
  (if (not (seq? code))
    (diff-term code inputs-map)


    ;; Next, there's the possibility that "code" is an application of a "special" function,
    ;; like 'let' or 'if' or 'cond' or 'fn'. Check the special-map to see if it's in there
    ;; and if so, just send in the args and inputs-map.
    (let [func (first code)]
      (if (contains? special-map func)
        ((get special-map func) (rest code) inputs-map)


        ;; if it's not a special function, then we proceed to differentiate all the arguments
        ;; to this function. For simple functions, we send in args and dargs only;
        ;; they don't need to know inputs-map.
        (let [tmp (map #(diff-expr % inputs-map) (rest code))
              args (map first tmp)
              dargs (map second tmp)]


          ;; two possibilities left (not accounting for the possibility of an error/unknown function).
          ;; This might be a simple function we've already implemented; check in funcs-map for those.
          (if (contains? funcs-map func)
            ((get funcs-map func) args dargs)


            ;; otherwise, this is an anonymous function defined within the code somewhere.
            ;; So differentiate the function itself, take the second return value
            ;; (which will be the derivative of the function, the first being the function itself)
            ;; and apply that function's derivative to the arguments and their derivatives.
            (try
              (let [diff-fn (second (diff-expr func inputs-map))]
                `(~diff-fn ~@args ~@dargs))


              ;; if there was an error applying this function, catch the error
              ;; and add the function onto it.
              (catch Exception e
                (throw (Exception. (str "Error differentiating function: " func
                                        ".\n" (.getMessage e))))))))))))






;; Differentiating a single term is pretty simple (for now).
;; Return a vector consisting of the term and its derivative.

;; If that term is a number, its derivative is zero.
;; If the term is a vector, map through its individual elements.
;; If the term is in inputs-map, which is a mapping going from
;; symbols like 'x to their dual symbols 'dx, then get the corresponding element
;; in inputs-map. Otherwise, for now, throw an error.

;; Note: the nil? check in there probably looks needlessly paranoid,
;; but I can imagine situation in which that might happen.
;; For instance, '(fn [x]) is perfectly valid Clojure code,
;; as is '(if x y).
;; Most likely, the code written to differentiate stuff like this
;; will end up differentiating nil, unless it's paranoid about checks.

(defn const? [term]
  (or (number? term)
      (keyword? term)))

(defn diff-term [term inputs-map]
  (cond

    (const? term)
    [term 0]

    (contains? inputs-map term)
    [term (get inputs-map term)]

    (nil? term)
    [nil nil]

    (vector? term)
    (diff-expr `(~'vector ~@term) inputs-map)

    (map? term)
    (diff-expr `(~'hash-map ~@term) inputs-map)

    (set? term)
    (diff-expr `(~'set ~@term) inputs-map)

    :else
    (throw (Exception. (str "Unknown term: " term)))))














;;; Some helper functions for autodifferentiation:


;; Filter-sum is a particularly useful function that makes outputted code much
;; more readable. It simply takes in a list of terms, filters out the zeros,
;; and then returns code that applies + to the non-zero terms.
;; However, if there are 0 terms, it returns 0 directly, and if there's only 1 term,
;; it returns that 1 term directly.
(defn filter-sum [terms]
  (let [terms (filter #(not= 0 %) terms)
        C (count terms)]
    (cond
      (= C 0) 0
      (= C 1) (first terms)
      :else `(~'+ ~@terms))))

;; Filter-minus does a similar thing.
(defn filter-minus [terms]
  (let [terms (filter #(not= 0 %) terms)
        C (count terms)]
    (cond
      (= C 0) 0
      (= C 1) (let [term (first terms)]
                (if (number? term)
                  (- term)
                  `(~'- ~term)))
      :else `(~'- ~@terms))))




;; The below function 'simplify' is a tricky one.
;; I wouldn't pay too much attention on the first readthrough.
;; It takes in a function 'func' and a set of variables,
;; and it searches through these variables to see if they're "easy". If they're "easy",
;; it does nothing to them. If they're not "easy", then it surrounds 'func' with a let-block
;; in which these variables are given temporary names.

;; Why? Duplication. If the user sends in the code
;; (* (expensive-function x) y z)
;; then we want to output
;; (let [tmp (expensive-function x)] (* derivative y z) (* tmp dy z) (* tmp y dz))
;; rather than call the expensive function multiple times unnecessarily.
;; Here 'x, 'y, and 'z are "easy", but (expensive-function x) is not "easy",
;; so it is not duplicated: it gets assigned a temporary variable name.

;; One slightly useful thing to do, to improve efficiency zero but readability
;; and testing substantially, would be to make it so that we don't sustitute for not-easy
;; variables if they're actually only used once. But that would probably require a high
;; degree of "looking inside", and may in general be not exactly possible I guess?

(defn easy? [x]
  (or (const? x) (symbol? x)))

(defn simplify [func & variables]
  (let [not-easy (filter #(not (easy? %)) (apply concat variables))
        new-var-names (map #(my-gensym "tmp" %) (range 1 (+ (count not-easy) 1)))
        not-easy-map (zipmap not-easy new-var-names)


        ;; for each variable, either replace the variable with its corresponding entry
        ;; in not-easy-map, or leave it as it is
        transformed-variables (map (fn [lst]
                                     (map #(if (contains? not-easy-map %)
                                             (get not-easy-map %)
                                             %)
                                          lst))
                                   variables)]

    ;; if there's nothing that isn't easy, we don't need a let-block.
    (if (= (count not-easy) 0)
      (apply func variables)

      ;; if there are some not-easy variables, then the below
      ;; sets up a let-statement that declares them, and then uses
      ;; in the function application the transformed variables instead.
      `(~'let [~@(interleave (vals not-easy-map) (keys not-easy-map))]
              ~(apply func transformed-variables)))))


;(simplify
;  (fn [one two] `(+ ~@one ~@two))
;  '(x y z (+ 2 3 5))
;  '(12 13 14 (+ a b c)))









;; Now for some simple derivatives of mathematical functions.
;; Each function must take in two arguments - args and inputs-map.
;; Args is obviously the arguments - inputs-map contains the mapping
;; from symbols to the symbols of their duals.
;; For instance, if we're differentiating a function of x and y,
;; inputs-map might be the map 'x -> 'dx, 'y -> 'dy.

;; Generally, these functions begin with the assignment
;; [args dargs] (differentiate-arguments args inputs-map)
;; which assigns 'args' to be the list of arguments to the function in question
;; and 'dargs' to be the derivatives of those arguments,
;; the list of corresponding dual numbers. Each differentiation
;; then makes use of 'args' and 'dargs' in its own idiomatic way.

;; These functions return a vector of two elements.
;; The first element is the return value of the function in question,
;; and the second element is its derivative, its dual.
;; For instance, (diff-expr '(+ x y) '{x dx, y dy})
;; should return ['(+ x y) '(+ dx dy)].

;; In some cases (specifically, *, -, *, and /) a good degree of
;; simplification is done to stop the expressions from becoming unwieldy.
;; This is partly for the purpose of speeding execution/compilation
;; but TBH mostly because it makes debugging and unit tests
;; much easier to wade through.


(defn diff-plus [args dargs]
  (let [summer (fn [args dargs] [(filter-sum args) (filter-sum dargs)])]
    (simplify summer args dargs)))

(defn diff-minus [args dargs]
  (let [subtractor (fn [args dargs] [(filter-minus args) (filter-minus dargs)])]
    (simplify subtractor args dargs)))

;(diff-expr '(+ x (- y z 2)) '{x dx y dy z dz})




;; Times needs some filtering too. We don't want (* x 2 3 5) to balloon out -
;; so I just filter out all the terms whose derivatives are clearly going to be zero.
;; The following function "sum-products" only adds the terms whose dargs are not zero.

(defn diff-times-sum-products [args dargs]
  (let [D (count dargs)
        not-zero-darg-nums (filter #(not= 0 (nth dargs %)) (range 0 D))
        products (map
                   (fn [d]
                     `(~'*
                        ~@(take d args)
                        ~(nth dargs d)
                        ~@(take-last (- D d 1) args)))
                   not-zero-darg-nums)]
    `[(~'* ~@args) ~(filter-sum products)]))

(defn diff-times [args dargs]
  (let [D (count dargs)]
    (cond
      (= D 0)
      [1 0]
      (= D 1)
      `[~(first args) ~(first dargs)]

      :else
      (simplify diff-times-sum-products args dargs))))

;(diff-expr '(* x (+ 4 4)) {'x 'dx})
;(diff-expr '(* (+ 1 1 1 1 1 1) x x) '{x dx})




;; Division has another version of the sum-products from above, but more complex, with some extra terms.

;; It's a little messy, but it has to be, I think.
;; Repeated division is inherently a little messy.
;; Basically, what you need to know is:

;; d/dy(x/y/z) = (x/1/z) * d/dy(1/y/1) = (x/z) * (-1/y^2).

;; We can handle every element this way
;; other than the first one, which doesn't have a (-1/x^2) term.
;; So, implementing that:

(defn diff-div-sum-products [args dargs]
  (let [D (count dargs)
        not-zero-nums (filter #(not= 0 (nth dargs %)) (range 0 D))
        not-zero-args (map #(nth args %) not-zero-nums)
        not-zero-dargs (map #(nth dargs %) not-zero-nums)

        ;; as for the rest of the terms:

        divisions (map
                    (fn [d]
                      (if (= d 0)
                        `(~'/ 1 ~@(rest args))
                        `(~'/
                           ~@(take d args)
                           ~@(take-last (- D d 1) args)
                           1))) ;; this 1 is a little weird, but it needs to be here for the case of when there are only 2 arguments.
                    not-zero-nums)

        products (map
                   (fn [d arg-d diff-d div-d]
                     (if (= d 0)
                       `(~'* ~diff-d ~div-d)
                       `(~'* ~diff-d ~div-d
                             (~'pow ~arg-d -2))))
                   not-zero-nums not-zero-args not-zero-dargs divisions)]
    `[(~'/ ~@args) ~(filter-sum products)]))

(defn diff-div [args dargs]
  (let [D (count dargs)]
    (cond
      (= D 0)
      [1 0]

      (= D 1) ;; (/ 100) evaluates to 1/100. 1/x should be -1/x^2, but 1/2 should just be 0.
      `[(~'/ ~(first args))
        ~(if (= (first dargs) 0)
           0
           `(~'* ~(first dargs)
                 (~'pow ~(first args) -2)))]

      :else
      (simplify diff-div-sum-products args dargs))))

;(diff-expr '(/ 2 x) '{x dx y dy})
;(diff-expr '(/ x) {'x 'dx})
;(diff-expr '(/ x 2) '{x dx})
;(diff-expr '(/ x 3 4 5) '{x dx y dy})
;(diff-expr '(/ x y) '{x dx y dy z dz})


(defn diff-exp [args dargs]
  (let [exponentiator (fn [args dargs]
                        (let [arg (first args) darg (first dargs)]
                          `[(~'exp ~arg)
                            (~'* ~darg (~'exp ~arg))]))]
    (simplify exponentiator args dargs)))

;(diff-expr '(exp (+ x y)) '{x dx y dy})

(defn diff-pow [args dargs]
  (let [powerer (fn [args dargs]
                  (let [arg1 (first args) arg2 (second args)
                        darg1 (first dargs) darg2 (second dargs)]
                    `[(~'pow ~arg1 ~arg2)

                      ~(filter-sum
                         (list (if (= darg1 0) 0
                                 `(~'* ~darg1 ~arg2 (~'pow ~arg1 (~'- ~arg2 1))))
                               (if (= darg2 0) 0
                                 `(~'* ~darg2 (~'pow ~arg1 ~arg2) (~'log ~arg1)))))]))]

    (simplify powerer args dargs)))

;(diff-expr '(pow x y) '{x dx, y dy})






(def math-funcs-map
  {
    '+ diff-plus '- diff-minus '* diff-times '/ diff-div
    'exp diff-exp 'pow diff-pow

    `+ diff-plus `- diff-minus `* diff-times `/ diff-div

    })















;; the below simple function is used
;; for any operators whose (sub)gradients are always constant
;; (e.g. all comparators).
(defn const-grad [func]
  (fn [args dargs]
    `[(~func ~@args) 0]))


;; Here are all the functions with constant gradient.
;; This code just builds up a map that goes from the function's symbol
;; to a const-grad function defined as above - i.e. a map
;; with entries of the form {'< (const-grad '<)}.
;; This will be integrated into funcs-map, the list of all
;; differentiable non-special-forms.

(def const-funcs-map
  (let [const-grad-funcs
        (concat
          '(< > = <= >= not=
              nil? empty? contains? seq?)
          `(< > = <= >= not=
              nil? empty? contains? seq?))]
    (apply hash-map
           (interleave const-grad-funcs
                       (map #(const-grad %) const-grad-funcs)))))


;; The below simple function is use
;; for any operators whose subgradients are "trivial".
;; For instance, the derivative of (first x), where x is a list,
;; is simply [(first x) (first dx)]: we just apply the function
;; to both its args & dargs equally. A "trivial" function is just
;; when we evaluate the gradient exactly the same as the function itself.

(defn triv-grad [func]
  (fn [args dargs]
    `[(~func ~@args) (~func ~@dargs)]))



;; similarly, we build up a map of all of the functions
;; with trivial gradients:
(def triv-funcs-map
  (let [triv-grad-funcs
        (concat
          '(
             list vector set hash-map
             first second next last rest
             cons conj concat seq)
          `(
             list vector set hash-map
             first second next last rest
             cons conj concat seq)
          )]

    (apply hash-map
           (interleave triv-grad-funcs
                       (map #(triv-grad %) triv-grad-funcs)))))







;; finally, some miscellaneous functions - functions
;; that are neither math functions, nor constant, nor trivial.

(defn diff-nth [args dargs]
  (let [lst (first args)
        dlst (first dargs)
        n (second args)]
    `[(~'nth ~lst ~n) (~'nth ~dlst ~n)]))

;(diff-expr '(nth (list x y z) 2) '{x dx y dy z dz})


(def other-funcs-map
  {'nth diff-nth `nth diff-nth})




;; Finally, the construct 'funcs-map' combines the math functions,
;; trivial functions, constant functions, and other functions into one map:
(def funcs-map
  (merge math-funcs-map
         const-funcs-map
         triv-funcs-map
         other-funcs-map))













;;; Derivatives of special functions follow...




;; if is a simple one:
(defn diff-if [args inputs-map]
  (let [condn (first args)

        if-true (diff-expr (first (rest args)) inputs-map)
        if-false (diff-expr (second (rest args)) inputs-map)]

    `(~'if ~condn ~if-true ~if-false)))

;(pprint (diff-expr '(if (< 2 3) (+ x (+ y 1)) 66) {'x 'dx, 'y 'dy}))
;(diff-expr '(if (< x y) (+ x y) (- x y)) '{x dx, y dy})

;; when is quite similar to if, but with multiple statements to do
;; afterwards, and no false condition:
(defn diff-when [args inputs-map]
  (let [condn (first args)
        if-true (map #(diff-expr % inputs-map) (rest args))]
    `(~'when ~condn ~@if-true)))

;; do is trivial to transform; just transform all of the arguments individually:
(defn diff-do [args inputs-map]
  `(~'do ~@(map #(diff-expr % inputs-map) args)))

(defn diff-cond [args inputs-map]
  (let [tests (take-nth 2 args) ;; the even elements are the tests, the last of which is :else
        returns (take-nth 2 (rest args)) ;; the odd ones are the corresponding return values

        new-returns (map #(diff-expr % inputs-map) returns)]

    ;; then just interleave the tests with the new returns:
    `(~'cond ~@(interleave tests new-returns))))

;(diff-expr '(cond (= x 2) [10] (= x 3) x :else (* x x)) '{x dx})

(defn diff-case [args inputs-map]
  (let [expr (first args)
        tests (take-nth 2 (rest args))
        returns (take-nth 2 (drop 2 args))

        new-returns (map #(diff-expr % inputs-map) returns)]
    ~(~'cond ~expr ~@(interleave tests new-returns))))





(defn diff-let* [args inputs-map]
  (let [defs (first args)
        body (second args)

        variables (take-nth 2 defs) ;; get even elements
        values (take-nth 2 (rest defs)) ;; get odd elements

        d-variables (map #(my-gensym "d" %) variables)
        inputs-map (merge inputs-map (zipmap variables d-variables))

        d-values (map #(diff-expr % inputs-map) values)

        variables-and-d-variables (map #(into [] %) (partition 2 (interleave variables d-variables)))]

    `(~'let [~@(interleave variables-and-d-variables d-values)]
            ~(diff-expr body inputs-map))))

;(pprint (diff-expr '(let* [x 3 y (* x 2)] (+ x y)) nil))
;(diff-expr '(let* [f #(* %1 %2)] (f x x)) nil)


(defn diff-let [args inputs-map]
  (let [defs (first args)
        body (second args)]
    (diff-let* (list (destructure defs) body) inputs-map)))

;(diff-expr '(let [[x y] [2 3]] (* x y)) nil)





;; diffing loop is almost exactly the same as diffing let...

(defn diff-loop* [args inputs-map]
  (let [defs (first args)
        body (second args)

        variables (take-nth 2 defs) ;; get even elements
        values (take-nth 2 (rest defs)) ;; get odd elements

        d-variables (map #(my-gensym "d" %) variables)
        inputs-map (merge inputs-map (zipmap variables d-variables))

        d-values (map #(diff-expr % inputs-map) values)

        variables-and-d-variables (map #(into [] %) (partition 2 (interleave variables d-variables)))]

    `(~'loop [~@(interleave variables-and-d-variables d-values)]
             ~(diff-expr body inputs-map))))


(defn diff-loop [args inputs-map]
  (let [defs (first args)
        body (second args)]
    (diff-loop* (list (destructure defs) body) inputs-map)))



;; differentiating recur simply requires replacing it with another recur
;; with additional differential return values:

(defn diff-recur [args inputs-map]
  `(~'recur ~@(map #(diff-expr % inputs-map) args)))




;; repeat is pretty easy; just map into the thing to repeat:
(defn diff-repeat [args inputs-map]
  (let [times (first args)
        to-repeat (second args)
        d-to-repeat (diff-expr to-repeat inputs-map)]
    `[(~'repeat ~times ~to-repeat)
      (~'repeat ~times ~(second d-to-repeat))]))

;(diff-expr '(repeat 10 x) '{x dx})

;; repeatedly is only slightly harder. Differentiate the function in question,
;; and once we have its derivative, repeat it, yielding a list of values & derivatives.
;; Then just return a single vector of values & derivatives like normal:
(defn diff-repeatedly [args inputs-map]
  (let [times (first args)
        to-repeat (second (diff-expr (second args) inputs-map))
        results (my-gensym "results")]
    `(~'let [~results (~'repeatedly ~times ~to-repeat)]
            [(~'map ~'first ~results) (~'map ~'second ~results)])))

;(diff-expr '(repeatedly 10 #(* 3 3)) nil)



;; First, some function is defined, and we return
;; a FUNCTION that also takes in gradient values
;; and that returns the gradient of that function.
;; next, when we actually call that function, it returns
;; code that automatically computes the derivative of what we call it with
;; and calculates the derivative of the inner function.

(defn diff-fn* [args inputs-map]
  (if (vector? (first args))
    (diff-fn* (conj args 'FN) inputs-map)
    (let [fn-name (first args)
          new-fn-name (my-gensym "d" (first args))
          params (second args)
          body (drop 2 args)

          ;; first thing: add differential params.
          d-params (map #(my-gensym "d" %) params)
          inputs-map (merge inputs-map (zipmap params d-params))

          ;; Next, differentiate the body of the function:
          new-body (map #(diff-expr % inputs-map) body)]

      `[(~'fn* ~fn-name [~@params] ~@body)
        (~'fn* ~new-fn-name [~@params ~@d-params] ~@new-body)])))





;(defn diff-apply [args inputs-map]
;  (let [func (first args)
;        lst (second args)
;
;        d-lst (diff-expr lst inputs-map)
;        args (first d-lst)
;        dargs (second d-lst)]
;
;    (if (not (contains? funcs-map func))
;      (let [diffed (second (diff-expr func inputs-map))]
;        `(~'apply ~diffed (~'concat ~args ~dargs)))
;
;
;      (let [arg (my-gensym "arg")
;            darg (my-gensym "darg")
;            result (my-gensym "result")
;            dresult (my-gensym "dresult")]
;
;        `(~'if (~'= (~'count ~args) 1)
;
;               ((~'fn [~arg ~darg] ~((get funcs-map func) (list arg) (list darg)))
;                (~'first ~args) (~'first ~dargs))
;
;               ;; otherwise, just turn it into reduce (for now):
;               ~(diff-reduce (list func (list 'map 'vector args dargs)) inputs-map))))))
;  (~'reduce
;    (~'fn [[~result ~dresult] [~arg ~darg]]
;          ~((get funcs-map func)
;            (list arg result)
;            (list darg dresult)))
;    (~'map ~'vector ~args ~dargs)))))))




;(diff-expr '(apply (fn [x y z] (+ x y z)) (list a a a)) {'a 'da})
;(pprint (diff-expr '(apply * (list x y z)) '{x dx y dy z dz}))
;(pprint (diff-expr '(apply * (repeat 10 x)) '{x dx}))



(defn diff-map [args inputs-map]
  (let [func (first args)
        lists (rest args)
        D (count lists)

        d-lists (map #(diff-expr % inputs-map) lists)
        args (map first d-lists)
        dargs (map second d-lists)

        diffed-fn (if (contains? funcs-map func)
                    (let [vars (map #(my-gensym "tmp" %) (range 1 (+ D 1)))
                          dvars (map #(my-gensym "dtmp" %) (range 1 (+ D 1)))]
                      `(~'fn [~@vars ~@dvars] ~((get funcs-map func) vars dvars)))

                    (second (diff-expr func inputs-map)))]

    `(~'map ~diffed-fn ~@args ~@dargs)))

;(pprint (diff-expr '(map (fn [x y] (* x y)) (list a a) (list 2 2)) '{a da}))
;(pprint (diff-expr '(map * (list a a) (list 2 2)) '{a da}))


(defn diff-reduce [args inputs-map]
  (let [initial? (= (count args) 3)
        func (first args)

        initial (if initial? (second args))
        d-initial (if initial? (diff-expr initial inputs-map))

        lst (if initial? (nth args 2) (second args))
        d-lst (diff-expr lst inputs-map)
        args (first d-lst)
        dargs (second d-lst)

        arg (my-gensym "arg")
        darg (my-gensym "darg")
        result (my-gensym "result")
        dresult (my-gensym "dresult")

        diffed-fn (if (contains? funcs-map func)
                    `(~'fn [[~result ~dresult] [~arg ~darg]]
                           ~((get funcs-map func)
                             (list arg result)
                             (list darg dresult)))

                    `(~'fn [[~result ~dresult] [~arg ~darg]]
                           (~(second (diff-expr func inputs-map))
                             ~result ~dresult
                             ~arg ~darg)))

        args-and-dargs (list 'map 'vector args dargs)]

    (if initial?
      `(~'reduce ~diffed-fn ~d-initial ~args-and-dargs)
      `(~'reduce ~diffed-fn ~args-and-dargs))))

;(diff-expr '(reduce + (list x x x)) '{x dx})
;(diff-expr '(reduce (fn [x y] (+ x y)) 10 (list a a a a)) '{a da})

(defn diff-filter [args inputs-map]
  (let [func (first args)
        lst (second args)
        d-lst (diff-expr lst inputs-map)
        args (first d-lst)
        dargs (second d-lst)

        filtered (my-gensym "filtered")
        arg (my-gensym "arg")
        darg (my-gensym "darg")]

    `(~'let [~filtered (~'filter
                         (~'fn [[~arg ~darg]] (~func ~arg))
                         (~'map ~'vector ~args ~dargs))]
            [(~'map ~'first ~filtered) (~'map ~'second ~filtered)])))

;(pprint (diff-expr '(filter #(> % 0) (list (- x) 0 1 2 x)) '{x dx}))





;; Special forms do not need to be included twice, because e.g. (= 'if `if).
;; They're not functions in clojure.core; they're part of the compiler.
;; So quoting doesn't work on them the same way it works on everything else.

(def special-forms {'if diff-if 'do diff-do
                    'let* diff-let* 'fn* diff-fn*
                    'loop* diff-loop 'recur diff-recur})


;; Core functions do need two entries each.
(def core-functions
  {
    'when diff-when `when diff-when
    'cond diff-cond `cond diff-cond
    'case diff-case `case diff-case
    'let diff-let `let diff-let
    'loop diff-loop `loop diff-loop
    'repeat diff-repeat `repeat diff-repeat
    'repeatedly diff-repeatedly `repeatedly diff-repeatedly
    ;'fn diff-fn `fn diff-fn
    ;'apply diff-apply `apply diff-apply
    'map diff-map `map diff-map
    'reduce diff-reduce `reduce diff-reduce
    'filter diff-filter `filter diff-filter
    })

(def special-map
  (merge special-forms core-functions))



